﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{

    public HingeJoint foldJoint;

    private int numberOfSteps = 8;
    private float maxStepHeight = 10f;

    public GameObject ladderStep1;
    public GameObject ladderStep2;
    public GameObject ladderStep3;
    public GameObject ladderStep4;
    public GameObject ladderStep5;
    public GameObject ladderStep6;
    public GameObject ladderStep7;
    public GameObject ladderStep8;

    private Ladder ladder;
    private Folder folder;

    public void Start()
    {
        ladder = new Ladder(new GameObject[] { ladderStep1, ladderStep2, ladderStep3, ladderStep4, ladderStep5, ladderStep6, ladderStep7, ladderStep8}, maxStepHeight);
        folder = new Folder(0, 20);
    }

    private float ladderHeight = 0;
    private float ladderVelocity = 10;

    public void Update()
    {
        UpdateFold();
        UpdateLadder();
    }
    private void UpdateFold()
    {
        if (UniversalInput.GetButtonDown("X"))
        {
            if (folder.isClosed())
                folder.Open(foldJoint);
            else
                folder.Close(foldJoint);
        }
    }
    private void UpdateLadder()
    {
        bool ladderUp = UniversalInput.GetButton("Y");
        bool ladderDown = UniversalInput.GetButton("A");
        if (ladderUp && !ladderDown)
            ladderHeight += ladderVelocity * Time.deltaTime;
        if (ladderDown && !ladderUp)
            ladderHeight -= ladderVelocity * Time.deltaTime;
        ladderHeight = Math.Min(Math.Max(0, ladderHeight), maxStepHeight * (numberOfSteps - 1));
        ladder.SetHeight(ladderHeight);
    }

    private class Ladder
    {
        private GameObject[] steps;
        private readonly float maxStepHeight;

        /// <param name="steps">Steps on the ladder, ordered from bottom to top</param>
        public Ladder(GameObject[] steps, float maxStepHeight)
        {
            this.steps = steps;
            this.maxStepHeight = maxStepHeight;
        }

        public void SetHeight(float height)
        {
            int step = steps.Length - 1; // start at top
            while (step >= 0 && step <= steps.Length)
            {
                if (height >= maxStepHeight)
                {
                    steps[step].transform.localPosition = new Vector3(0, maxStepHeight, 0);
                    height -= maxStepHeight;
                    step--;
                }
                else if (height > 0)
                {
                    steps[step].transform.localPosition = new Vector3(0, height, 0);
                    height = 0;
                    step--;
                }
                else
                {
                    steps[step].transform.localPosition = new Vector3(0, 0, 0);
                    step--;
                }
            }
        }

    }
    private class Folder
    {

        private int openedAngle, closedAngle;
        private bool closed = false;

        public Folder(int openedAngle, int closedAngle)
        {
            this.openedAngle = openedAngle;
            this.closedAngle = closedAngle;
        }

        public void Close(HingeJoint joint)
        {
            closed = true;

            JointSpring spring = joint.spring;
            spring.targetPosition = closedAngle;
            joint.spring = spring;

            JointMotor motor = joint.motor;
            motor.targetVelocity = 100;
            joint.motor = motor;
        }
        public void Open(HingeJoint joint)
        {
            closed = false;

            JointSpring spring = joint.spring;
            spring.targetPosition = openedAngle;
            joint.spring = spring;

            JointMotor motor = joint.motor;
            motor.targetVelocity = -100;
            joint.motor = motor;
        }

        public bool isClosed()
        {
            return closed;
        }

    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Robot : MonoBehaviour
{

    public GameObject robotBase;
    
    public void Update()
    {
        Move();
    }

    private void Move()
    {
        robotBase.transform.Rotate(0, UniversalInput.GetAxis("Horizontal") * Time.deltaTime * 270, 0);
        robotBase.transform.Translate(0, 0, UniversalInput.GetAxis("Vertical") * Time.deltaTime * -4);
    }

    public void ResetPosition()
    {
        transform.SetPositionAndRotation(new Vector3(0, 0, 0), Quaternion.Euler(new Vector3(0, 0, 0)));
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    public Dropdown robot;
    public Dropdown mode;
    public Dropdown onScreenControls;

    public void PlayGame() {
        SpawnChooser.robot = GetText(robot);
        ScreenControlCreator.useScreenControls = GetText(onScreenControls).Equals("Yes");
        UniversalInput.useScreenControls = GetText(onScreenControls).Equals("Yes");
        SceneManager.LoadScene(GetText(mode));
    }

    private static string GetText(Dropdown d) {
        return d.options[d.value].text;
    }
}

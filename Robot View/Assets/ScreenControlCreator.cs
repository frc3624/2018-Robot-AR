﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenControlCreator : MonoBehaviour
{

    public static bool useScreenControls;

    public GameObject screenControlPrefab;

    void Start()
    {
        if (useScreenControls)
        {
            GameObject screenControls = Instantiate(screenControlPrefab) as GameObject;
        }
    }

}
